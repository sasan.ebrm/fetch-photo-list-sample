package com.sebrahimi.model

import java.io.Serializable


data class PhotoModel(
    val albumId: Int,
    val id: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
): Serializable