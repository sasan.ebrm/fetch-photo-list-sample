package com.sebrahimi.model.util

/**
 * DTO, Entity, etc data objects that can be mapped to a model should implement this interface
 * This method converts an object (DTO, Entity, etc) to a desired model to be passed to UI layer
 */
interface MappableToModel<T> {
    fun mapToModel(): T
}

fun <T> List<MappableToModel<T>>.mapToListOfModels() : List<T> {
    return map {
        it.mapToModel()
    }
}