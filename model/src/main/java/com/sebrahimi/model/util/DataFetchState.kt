package com.sebrahimi.model.util

/**
 * A wrapper class to identify the state of fetch effort
 * Can be used with any type of data source (Rest Api, Database, Local storage)
 */
sealed class DataFetchState<out T> {
    data class Success<T>(val data: T) : DataFetchState<T>()
    data class Failure(val error: Exception) : DataFetchState<Nothing>()

}

/**
 * Maps the content of [DataFetchState.Success] object to a model
 */
fun <T : MappableToModel<K>, K> DataFetchState<T>.mapDataToModel(): DataFetchState<K> {
    return when (this) {
        is DataFetchState.Failure -> this
        is DataFetchState.Success -> DataFetchState.Success(this.data.mapToModel())
    }
}

/**
 * Maps the a list of [DataFetchState.Success] objects to a list of models
 */
fun <T : List<MappableToModel<K>>, K> DataFetchState<T>.mapDataListToModel(): DataFetchState<List<K>> {
    return when (this) {
        is DataFetchState.Failure -> this
        is DataFetchState.Success -> DataFetchState.Success(this.data.mapToListOfModels())
    }
}
