package com.sebrahimi.repository.di.util

import com.sebrahimi.model.util.DataFetchState
import retrofit2.Response


/**
 * Extension function to make it possible to call process directly on a [Response] object
 */
fun <T> Response<T>.process(): DataFetchState<T> {
    return RetrofitHelper.processRetrofitResponse(this)
}