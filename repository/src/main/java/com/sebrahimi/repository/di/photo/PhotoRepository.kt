package com.sebrahimi.repository.di.photo

import com.sebrahimi.model.PhotoModel
import com.sebrahimi.model.util.DataFetchState
import com.sebrahimi.model.util.mapDataListToModel
import com.sebrahimi.network.dto.PhotoDTO
import com.sebrahimi.network.service.PhotoRestApi
import com.sebrahimi.repository.di.util.process
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class PhotoRepository @Inject constructor(
    private val photoRestApi: PhotoRestApi
) : IPhotoRepository() {

    private val cacheWindowSize = 100
    private val cachedPhotos = mutableListOf<PhotoDTO>()

    /**
     * This methods requests the datasource (RestApi) and returns a [Flow] list of [PhotoModel]
     * @param page the page number of data that should be fetched
     * @param pageSize the number of items per page
     * @return the [Flow] list of [PhotoModel]
     */
    override fun getPhotos(
        page: Int,
        pageSize: Int
    ): Flow<DataFetchState<List<PhotoModel>>> {
        return flow {
            delay(1300) // Fake delay
            emit(getPhotosFromDataSource(page, pageSize))
        }.map {
            // map DTOs to Models
            it.mapDataListToModel()
        }.map {
            if (it is DataFetchState.Success)
                return@map DataFetchState.Success(it.data)
            else
                return@map it
        }
            .flowOn(Dispatchers.IO)
    }

    /**
     * Checks if the data should be fetched from RestApi or Cached list
     * then returns the data from the appropriate data source
     */
    private suspend fun getPhotosFromDataSource(
        page: Int,
        pageSize: Int
    ): DataFetchState<List<PhotoDTO>> {
        return if (shouldFetchDataFromRestApi(page, pageSize)) {
            val result = photoRestApi.getPhotos().process()
            cacheResult(result)
            getCachedPhotos(page, pageSize)
        } else {
            getCachedPhotos(page, pageSize)
        }
    }

    /**
     * Saves new portion of photo (as much as [cacheWindowSize]) items into [cachedPhotos]
     */
    private fun cacheResult(photosList: DataFetchState<List<PhotoDTO>>) {
        if (photosList is DataFetchState.Success) {
            // TODO check if the data is not cached already
            cachedPhotos.addAll(photosList.data.filterIndexed { index, _ ->
                index >= cachedPhotos.size && index < cacheWindowSize + cachedPhotos.size
            })
        }
    }

    /**
     * returns the cached photo items in [cachedPhotos] list based on requested page number and page size
     */
    private fun getCachedPhotos(page: Int, pageSize: Int): DataFetchState<List<PhotoDTO>> =
        DataFetchState.Success(cachedPhotos.filterIndexed { index, _ ->
            return@filterIndexed (index >= (pageSize * page) - pageSize
                    &&
                    (index < pageSize * page))
        })


    private fun shouldFetchDataFromRestApi(page: Int, pageSize: Int): Boolean {
        if (cachedPhotos.size >= page * pageSize)
            return false
        return true
    }
}