package com.sebrahimi.repository.di.photo

import com.sebrahimi.model.PhotoModel
import com.sebrahimi.model.util.DataFetchState
import kotlinx.coroutines.flow.Flow

abstract class IPhotoRepository {
    abstract fun getPhotos(page: Int, pageSize: Int): Flow<DataFetchState<List<PhotoModel>>>
}