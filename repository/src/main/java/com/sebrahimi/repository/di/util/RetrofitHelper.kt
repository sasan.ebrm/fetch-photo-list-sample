package com.sebrahimi.repository.di.util

import com.sebrahimi.model.util.DataFetchState
import retrofit2.Response

object RetrofitHelper {

    /**
     * Checks the retorfit [Response] object to process the content of it and map to proper [DataFetchState] object
     */
    fun <K> processRetrofitResponse(retrofitResponse: Response<K>): DataFetchState<K> {
        return if (retrofitResponse.isSuccessful)
            if (retrofitResponse.body() != null) DataFetchState.Success(retrofitResponse.body()!!)
            else handleHttpError(retrofitResponse.code(), "Null Response Body")
        else handleHttpError(retrofitResponse.code(), retrofitResponse.message())
    }

    /**
     * Generates an [DataFetchState.Failure] object with a proper exception message
     */
    // TODO change error messages in a better format
    private fun handleHttpError(code: Int, message: String?): DataFetchState.Failure {
        var errorMessage = if (code >= 500) "Server Side\nError Code: $code"
        else if (code >= 400) "Error Code: $code"
        else "Unknown Error\nError Code: $code"
        message?.let {
            errorMessage += "\nDetail: $message"
        }
        return DataFetchState.Failure(Exception(errorMessage))
    }
}
