package com.sebrahimi.repository.di

import com.sebrahimi.network.service.PhotoRestApi
import com.sebrahimi.repository.di.photo.IPhotoRepository
import com.sebrahimi.repository.di.photo.PhotoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    fun providePhotoRepository(
        photoRestApi: PhotoRestApi
    ): IPhotoRepository{
        return PhotoRepository(photoRestApi)
    }

}