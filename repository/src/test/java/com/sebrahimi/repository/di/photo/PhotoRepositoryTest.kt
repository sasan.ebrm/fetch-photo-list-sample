package com.sebrahimi.repository.di.photo

import com.sebrahimi.model.util.DataFetchState
import com.sebrahimi.network.dto.PhotoDTO
import com.sebrahimi.network.service.PhotoRestApi
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import retrofit2.Response

class PhotoRepositoryTest {

    @Mock
    private lateinit var photoRestApi: PhotoRestApi

    private lateinit var photoRepository: IPhotoRepository

    private val LONG_LIST_OF_PHOTO_DTO =
        List(500) { PhotoDTO(it * 10, it, "This is item $it", "", "") }

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        photoRepository = PhotoRepository(photoRestApi)
    }

    @kotlinx.coroutines.ExperimentalCoroutinesApi
    @Test
    fun `A successful api call result in a single DataFetchState Success object`() = runTest {
        val data = LONG_LIST_OF_PHOTO_DTO
        `when`(photoRestApi.getPhotos()).thenReturn(
            Response.success(data)
        )

        val result = photoRepository.getPhotos(1, 20).toList()

        assert(result.size == 1)
        assert(result.first() is DataFetchState.Success)
    }


    @kotlinx.coroutines.ExperimentalCoroutinesApi
    @Test
    fun `request first page returns the data as expected`() = runTest {
        val data = LONG_LIST_OF_PHOTO_DTO
        `when`(photoRestApi.getPhotos()).thenReturn(
            Response.success(data)
        )
        var pageSize = 20
        var result = photoRepository.getPhotos(1, pageSize).single()

        assert(result is DataFetchState.Success)
        result as DataFetchState.Success
        assert(result.data.size == pageSize)
        result.data.forEachIndexed { index,photoModel ->
            assert(photoModel == data[index].mapToModel())
        }

    }

    @Test
    fun `request a page above the available data should return empty list`() = runTest {
        val data = LONG_LIST_OF_PHOTO_DTO
        `when`(photoRestApi.getPhotos()).thenReturn(
            Response.success(data)
        )
        var pageSize = 20
        var result = photoRepository.getPhotos(30, pageSize).single()
        assert(result is DataFetchState.Success)
        result as DataFetchState.Success
        assert(result.data.isEmpty())
    }

    // TODO Add more scenarios

}