package com.sebrahimi.fetchphotolist.application

import com.sebrahimi.shared.DIKeys
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Named(DIKeys.BASE_URL)
    fun provideBaseUrl(): String{
        return Constants.BASE_URL
    }
}