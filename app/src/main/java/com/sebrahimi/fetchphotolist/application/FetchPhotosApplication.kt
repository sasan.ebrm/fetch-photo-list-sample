package com.sebrahimi.fetchphotolist.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FetchPhotosApplication: Application()