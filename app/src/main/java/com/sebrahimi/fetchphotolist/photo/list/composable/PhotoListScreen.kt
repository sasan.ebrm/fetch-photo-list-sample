package com.sebrahimi.fetchphotolist.photo.list.composable
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.BottomCenter
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import com.sebrahimi.fetchphotolist.photo.list.PhotoListItem
import com.sebrahimi.fetchphotolist.showToast
import com.sebrahimi.model.PhotoModel
import kotlinx.coroutines.flow.Flow

/**
 * Renders the list of photos in a [LazyColumn]
 */
@Composable
fun PhotoList(photos: Flow<PagingData<PhotoModel>>, context: Context, onItemClicked: (photModel: PhotoModel) -> Unit) {
    val lazyPhotoItems: LazyPagingItems<PhotoModel> = photos.collectAsLazyPagingItems()
    LazyColumn {
        items(lazyPhotoItems) { item ->
            item?.let {
                PhotoListItem(photoModel = it, onItemClicked)
            }
        }
    }
    when (val currentState = lazyPhotoItems.loadState.append) {
        is LoadState.NotLoading -> { ProgressIndicator(false)}
        is LoadState.Loading ->  { ProgressIndicator(true) }
        is LoadState.Error ->  {
            ProgressIndicator(false)
            context.showToast("ERROR! ${currentState.error.message}", Toast.LENGTH_LONG)
        }
    }
}

@Composable
fun ProgressIndicator(visible: Boolean = false) {
    if(visible)
        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            CircularProgressIndicator(
                modifier = Modifier
                    .width(42.dp)
                    .height(42.dp)
                    .padding(8.dp)
                    .align(BottomCenter),
                strokeWidth = 4.dp
            )
        }
}