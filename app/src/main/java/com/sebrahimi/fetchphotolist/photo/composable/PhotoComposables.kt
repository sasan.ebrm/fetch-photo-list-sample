package com.sebrahimi.fetchphotolist.photo.composable

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow

@Composable
fun PhotoItemTitleText(title:String,color: Color = Color.Black, style: TextStyle = MaterialTheme.typography.h6, modifier: Modifier = Modifier){
    Text(
        modifier = modifier,
        text = title,
        color = color,
        maxLines = 2,
        style = style,
        overflow = TextOverflow.Ellipsis,
    )
}