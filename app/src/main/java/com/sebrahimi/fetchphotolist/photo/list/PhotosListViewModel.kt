package com.sebrahimi.fetchphotolist.photo.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn

import com.sebrahimi.model.PhotoModel
import com.sebrahimi.model.util.DataFetchState
import com.sebrahimi.repository.di.photo.PhotoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotosListViewModel @Inject constructor(
    private val photoRepository: PhotoRepository
) : ViewModel() {

    private val pageSize = 20

    val photos: Flow<PagingData<PhotoModel>> = Pager(PagingConfig(pageSize = pageSize)) {
        PhotoListPagingSource(photoRepository, pageSize)
    }
        .flow
        .cachedIn(viewModelScope)

}