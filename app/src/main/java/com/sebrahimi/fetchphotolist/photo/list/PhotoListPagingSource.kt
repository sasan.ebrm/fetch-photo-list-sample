package com.sebrahimi.fetchphotolist.photo.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.sebrahimi.model.PhotoModel
import com.sebrahimi.model.util.DataFetchState
import com.sebrahimi.repository.di.photo.PhotoRepository
import javax.inject.Inject

class PhotoListPagingSource @Inject constructor(
    private val photoRepository: PhotoRepository,
    private val pageSize: Int
) : PagingSource<Int, PhotoModel>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PhotoModel> {
        try {
            val page = params.key ?: 1
            var loadResult: LoadResult<Int, PhotoModel>? = null
            photoRepository.getPhotos(page, pageSize)
                .collect {
                    loadResult = when(it){
                        is DataFetchState.Failure -> {
                            LoadResult.Error(it.error)
                        }
                        is DataFetchState.Success -> {
                            LoadResult.Page(
                                data = it.data,
                                prevKey = if (page == 1) null else page.minus(1),
                                nextKey = if (it.data.isEmpty()) null else page.plus(1)
                            )
                        }
                    }
                }
            return loadResult!!
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, PhotoModel>): Int? {
        return state.anchorPosition?.let { position ->
            val page = state.closestPageToPosition(position)
            page?.prevKey?.minus(1) ?: page?.nextKey?.plus(1)
        }
    }
}
