package com.sebrahimi.fetchphotolist.photo.details

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.sebrahimi.fetchphotolist.photo.details.composable.PhotoDetailScreen
import com.sebrahimi.fetchphotolist.showToast
import com.sebrahimi.fetchphotolist.ui.theme.FetchPhotoListTheme
import com.sebrahimi.model.PhotoModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhotoDetailActivity : ComponentActivity() {

    private lateinit var photoModel: PhotoModel

    companion object {
        const val EXTRAS_KEY_PHOTO_MODEL = "EXTRAS_KEY_PHOTO_MODEL"
        fun start(context: Context, photoModel: PhotoModel) {
            val intent = Intent(context, PhotoDetailActivity::class.java)
            intent.putExtra(EXTRAS_KEY_PHOTO_MODEL, photoModel)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (extractExtras()) {
            setContent {
                FetchPhotoListTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background
                    ) {
                        PhotoDetailScreen(photoModel) {
                            finish()
                        }
                    }
                }
            }
        } else {
            showToast("PhotoModel is invalid")
            finish()
        }
    }

    /**
     *
     */
    private fun extractExtras(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.extras?.let {
                it.getSerializable(EXTRAS_KEY_PHOTO_MODEL, PhotoModel::class.java)?.let {it ->
                    photoModel = it
                     return true
                }
            }
        } else {
            intent.extras?.let {
                (it.getSerializable(EXTRAS_KEY_PHOTO_MODEL) as PhotoModel)?.let {it ->
                    photoModel = it
                    return true
                }
            }
        }
        return false
    }
}