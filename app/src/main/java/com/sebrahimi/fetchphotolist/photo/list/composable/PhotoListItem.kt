package com.sebrahimi.fetchphotolist.photo.list

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import coil.compose.rememberImagePainter
import com.sebrahimi.fetchphotolist.photo.composable.PhotoItemTitleText
import com.sebrahimi.fetchphotolist.ui.theme.Size.CARD_CORNER_RADIUS
import com.sebrahimi.fetchphotolist.ui.theme.Size.CARD_ELEVATION_LIGHT
import com.sebrahimi.fetchphotolist.ui.theme.Space
import com.sebrahimi.model.PhotoModel

@Composable
fun PhotoListItem(photoModel: PhotoModel, onItemClicked: (photoModel: PhotoModel) -> Unit) {

    val elementsAroundSpacing = Space.LARGE
    val thumbHeight = 120.dp
    val thumbWidth = 120.dp

    Card(
        modifier = Modifier
            .padding(
                elementsAroundSpacing
            )
            .fillMaxWidth(),
        shape = RoundedCornerShape(CARD_CORNER_RADIUS),
        elevation = CARD_ELEVATION_LIGHT,
    ) {
        Row(verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .clickable(
                    enabled = true,
                    onClickLabel = "Clickable image",
                    onClick = {
                        onItemClicked(photoModel)
                    }
                )
        ) {
            ThumbNailImage(
                photoModel.thumbnailUrl,
                modifier = Modifier
                    .height(thumbHeight)
                    .width(thumbWidth)
                    .zIndex(2f)
            )
            Column(
                modifier = Modifier
                    .padding(
                        Space.XLARGE,
                        0.dp,
                        Space.NORMAL,
                        0.dp
                    ),
                verticalArrangement = Arrangement.SpaceAround
            ) {
                PhotoItemTitleText(
                    photoModel.title,
                )
                Text(
                    text = "ID ${photoModel.id}, Album: ${photoModel.albumId}",
                    fontSize = 16.sp,
                    color = MaterialTheme.colors.secondary
                )
            }
        }
    }
}



@Composable
fun ThumbNailImage(
    imageUrl: String,
    modifier: Modifier
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(CARD_CORNER_RADIUS, CARD_CORNER_RADIUS, 0.dp, 0.dp),
        elevation = CARD_ELEVATION_LIGHT
    ) {
        Image(
            painter = rememberImagePainter(
                data = imageUrl,
                builder = {
                }
            ),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
    }
}
