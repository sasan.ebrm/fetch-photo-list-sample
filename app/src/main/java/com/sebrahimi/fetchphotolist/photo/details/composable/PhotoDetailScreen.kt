package com.sebrahimi.fetchphotolist.photo.details.composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import coil.compose.rememberImagePainter
import com.sebrahimi.fetchphotolist.photo.composable.PhotoItemTitleText
import com.sebrahimi.fetchphotolist.ui.theme.Size
import com.sebrahimi.fetchphotolist.ui.theme.Space
import com.sebrahimi.model.PhotoModel

@Composable
fun PhotoDetailScreen(photoModel: PhotoModel, onCloseClicked: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(
                Space.LARGE
            )
            .fillMaxWidth(),
        shape = RoundedCornerShape(Size.CARD_CORNER_RADIUS),
        elevation = Size.CARD_ELEVATION_LIGHT,
    ) {
        Column(
            modifier = Modifier
                .padding(Space.XLARGE)
            ,
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column {
                MainImage(
                    photoModel.url,
                    modifier = Modifier
                        .aspectRatio(1f)
                        .fillMaxWidth()
                        .zIndex(2f)
                )
                Spacer(modifier = Modifier.height(Space.XXLARGE))
                PhotoItemTitleText(title = photoModel.title)
                Spacer(modifier = Modifier.height(Space.LARGE))
                Text(
                    text = "ID ${photoModel.id}, Album: ${photoModel.albumId}",
                    fontSize = 18.sp,
                    color = MaterialTheme.colors.secondary
                )
            }
            Button(
                modifier = Modifier
                    .fillMaxWidth(),
                shape = RoundedCornerShape(Size.CTA_BUTTON_CORNER_RADIUS) ,onClick = onCloseClicked) {
                Text(text = "Close", modifier = Modifier.padding(Space.LARGE) )
            }
        }
    }
}


@Composable
fun MainImage(
    imageUrl: String,
    modifier: Modifier
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(Size.CARD_CORNER_RADIUS, Size.CARD_CORNER_RADIUS, Size.CARD_CORNER_RADIUS, 0.dp),
        elevation = Size.CARD_ELEVATION_LIGHT
    ) {
        Image(
            painter = rememberImagePainter(
                data = imageUrl,
                builder = {
                }
            ),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
    }
}