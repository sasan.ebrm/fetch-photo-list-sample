package com.sebrahimi.fetchphotolist.photo.list

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.sebrahimi.fetchphotolist.photo.details.PhotoDetailActivity
import com.sebrahimi.fetchphotolist.photo.list.composable.PhotoList
import com.sebrahimi.fetchphotolist.ui.theme.FetchPhotoListTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PhotoListsActivity : ComponentActivity() {

    private val viewModel by viewModels<PhotosListViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch{
            repeatOnLifecycle(Lifecycle.State.STARTED){

            }
        }
        setContent {
            FetchPhotoListTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    PhotoList(photos = viewModel.photos, context = this){photoModel ->
                        PhotoDetailActivity.start(this,photoModel)
                    }
                }
            }
        }
    }
}

