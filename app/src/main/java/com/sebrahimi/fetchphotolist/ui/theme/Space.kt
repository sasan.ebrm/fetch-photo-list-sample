package com.sebrahimi.fetchphotolist.ui.theme

import androidx.compose.ui.unit.dp

object Space {
    val TINY = 4.dp
    val SMALL = 8.dp
    val NORMAL = 12.dp
    val LARGE = 18.dp
    val XLARGE = 24.dp
    val XXLARGE = 30.dp
}