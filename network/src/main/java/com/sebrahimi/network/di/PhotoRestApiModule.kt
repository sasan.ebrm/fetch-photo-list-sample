package com.sebrahimi.network.di

import com.sebrahimi.network.service.PhotoRestApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object PhotoRestApiModule {

    @Provides
    fun providePhotoAPI(
        retrofit: Retrofit
    ): PhotoRestApi {
        return retrofit.create(PhotoRestApi::class.java)
    }

}