package com.sebrahimi.network.service

import com.sebrahimi.network.dto.PhotoDTO
import retrofit2.Response
import retrofit2.http.GET

interface PhotoRestApi {

    @GET("/photos")
    suspend fun getPhotos(): Response<List<PhotoDTO>>
}