package com.sebrahimi.network.dto

import com.sebrahimi.model.PhotoModel
import com.sebrahimi.model.util.MappableToModel

data class PhotoDTO(
    val albumId: Int?,
    val id: Int?,
    val title: String?,
    val url: String?,
    val thumbnailUrl: String?
): MappableToModel<PhotoModel> {
    override fun mapToModel(): PhotoModel {
        // TODO set valid image urls as placeholders instead of empty string
        return PhotoModel(
            this.albumId  ?: 0,
            this.id  ?: 0,
            this.title  ?: "UNKNOWN",
            this. url ?: "",
            this.thumbnailUrl  ?: ""
        )
    }

}