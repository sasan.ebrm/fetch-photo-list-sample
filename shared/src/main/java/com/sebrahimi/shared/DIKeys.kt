package com.sebrahimi.shared

/**
 * Variable names to be used while providing dependencies use Hilt
 * To be used with @Named annotation in Hilt providers
 */
object DIKeys {
    const val BASE_URL = "BASE_URL"
}