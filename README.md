
# Fetch Photos List Sample App

A sample Android application that fetches a list of items from Rest Api and shows them in an infinite list.

## Demo
<img src="https://gitlab.com/sasan.ebrm/fetch-photo-list-sample/-/raw/main/readme/demo.gif?raw=true" width="300">

## Architecture

The project has been divided to some smaller modules as shown bellow.

<img src="https://gitlab.com/sasan.ebrm/fetch-photo-list-sample/-/raw/main/readme/app_architecture.png" width="700">


## What solved well in the project

- Since there is no pagination in the Rest Api and we get 5000 records in the response, a simple caching logic has been provided in the Repository to prevent sending numerous unnecessary records to the view-model, and also to prevent unnecessary network requests.
> In the repository the records are saved in a list to be reused in the next calls (requesting more data from view model), but it stores only a limited number of records (since storing 5000 items needs much memory), the ones that possibly would be needed for the next couple of requests.
- Breaking down the project into smaller modules (although the project is a small one) that keeps the project structure clean, makes testing easier and decreases build time.

## What would be next to improve it

- **The lazyColumn should be improved** since if user scroll down and while doing fetch request an Error happens, scrolling down/up won't retry to fetch more records anymore. Also rendering photos in the list seems laggy. These will leave a negative experience from User's perspective.
- **The codebase should be more covered by test.** There are few tests written as examples for the repository, but the whole project should be covered and well tested.
- **There are still bugs** that user can face while using the app. As an example, the scenario of scrolling down over all 5000 records and reaching the end of the list has not been handled properly. Also if the data coming from server has no image/thumbnail url, there is no placeholder image url to render instead of images and it doesn't look nice in User's eyes. 








